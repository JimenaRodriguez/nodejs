function inicializar(route, manejador){
  var server = require('http').createServer();
  var url = require('url');
  function control(petic, resp) {
    var pathName = url.parse(petic.url).pathname;
    console.log('Peticion Recibida');
    route(manejador, pathName, resp);
    resp.writeHead(200,
          {'content-type': 'text/plain'}
        );
    resp.write('Peticion recibida en el servidor' + pathName);
    //resp.end();
  }
  server.on('request', control).listen(8080);
  console.log('Servidor Inicializado');
}

exports.inicializar = inicializar;
